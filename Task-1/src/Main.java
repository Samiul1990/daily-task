public class Main {

    public static void main(String[] args) {

        Book book1, book2, book3, book4, book5;

        book1 = new Book(101, 1001, "abc", "abcd", "15/02/2000");
        book2 = new Book(102,1002,"bcd", "bcde","23/03/2001");
        book3 = new Book(103, 1003, "def", "fghj", "09/09/2008");
        book4 = new Book(104, 1004, "asd", "sdfg", "11/06/1980");
        book5 = new Book(105, 1005, "xyz", "zxcv", "25/02/2010");

        System.out.println(book1.ToString());
        System.out.println(book2.ToString());
        System.out.println(book3.ToString());
        System.out.println(book4.ToString());
        System.out.println(book5.ToString());

        /*
        ArrayList<Book> bookList =new ArrayList();
        bookList.add(new Book(1, "Horton", "Beginning Java", "64683BB", new Date(2002,02,02)));
        bookList.add(new Book(2, "Hasan", "JavaScript", "05127AA", new Date(2015,03,11)));
        bookList.add(new Book(3, "Sarker", "Management", "456LK9", new Date(2016,01,22)));
        bookList.add(new Book(4, "Abid", "Database", "123QWm", new Date(2009,03,09)));
        bookList.add(new Book(5,"Mehedi","English","432QQi", new Date(2010,01,13)));

        bookList.forEach((n) -> System.out.println(n.getBookName()+"\t"+"by"+"\t"+n.getBookAuthor()));
        */

    }
}
