import java.util.Date;

public class Book {
    private int ISBN, bookId;
    private String bookAuthor;
    private String bookName;
    private String publishedDate;

    //public Date publishedDate;

    //Constructor
    public Book(int bookId, int ISBN, String bookAuthor, String bookName, String publishedDate)
    //public Book(int bookId, int ISBN, String bookAuthor, String bookName, Date publishedDate)
    {
        this.bookId = bookId;
        this.ISBN = ISBN;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.publishedDate = publishedDate;
    }

    //getter
    public  int getBookId(){
        return bookId;
    }
    public  int getIsbn(){
        return ISBN;
    }
    public  String getBookName(){
        return bookName;
    }
    public  String getBookAuthor(){
        return bookAuthor;
    }
    public String getPublishedDate(){
        return publishedDate;
    }


    /*
    public date getPublishedDate(){
        return publishedDate;
    }
    */


    //Setter
    public void setBookId(int bookId){
        this.bookId = bookId;
    }
    public void setIsbn(int ISBN){
        this.ISBN = ISBN;
    }
    public void setBookName(String bookName){
        this.bookName = bookName;
    }
    public void setBookAuthor(String bookAuthor){
        this.bookAuthor = bookAuthor;
    }
    public void setPublishedDate(String publishedDate){
        this.publishedDate = publishedDate;
    }


    public String ToString(){

        /*return ("bookId " +bookId+"\n"+"BookNane "+bookName +"\n"+ "BookAuthor "+bookAuthor+"\n"+
                "ISBN "+ISBN+"\n"+"PublishedDate "+publishedDate +"\n\n");
        */

        return ("BookNane= " + bookName + "\n" + "BookAuthor= " + bookAuthor+"\n\n");
    }

}


